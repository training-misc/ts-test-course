import { Server } from '../../app/Server/Server';
import { LoginHandler } from '../../app/Handlers/LoginHandler';
import { Authorizer } from '../../app/Authorization/Authorizer';
import { DataHandler } from '../../app/Handlers/DataHandler';
import { UsersDBAccess } from '../../app/Data/UsersDBAccess';

jest.mock('../../app/Handlers/LoginHandler');
jest.mock('../../app/Handlers/DataHandler');
jest.mock('../../app/Authorization/Authorizer');

//# 1 arguments for create server
const requestMock = {
  url: ''
};
const responseMock = {
  end: jest.fn(),
  setHeader: jest.fn()
};
const listenMock = {
  listen: jest.fn()
};

// #1 mock create server
jest.mock('http', () => ({
  createServer: (cb: any) => {
    cb(requestMock, responseMock)
    return listenMock;
  }
}));

describe('Server test suite', () => {
  afterEach(() => {
    jest.clearAllMocks();
  })

  //#1 line 10-11, 13, 32 -35
  it('should create server on port 8080', () => {
    new Server().startServer();
    expect(listenMock.listen).toBeCalledWith(8080);
    expect(responseMock.end).toBeCalled();
  })

  //#2 line 17 -20
  it('should handle login requests', () => {
    requestMock.url = 'http://localhost:8080/login';
    new Server().startServer();
    const handleRequestSpy = jest.spyOn(LoginHandler.prototype, 'handleRequest');
    expect(handleRequestSpy).toBeCalled();
    expect(LoginHandler).toBeCalledWith(requestMock, responseMock, expect.any(Authorizer))
  });

  //#2 line 21 -23
  it('should handle data requests', () => {
    requestMock.url = 'http://localhost:8080/users'
    new Server().startServer();
    expect(DataHandler).toBeCalledWith(requestMock, responseMock, expect.any(Authorizer), expect.any(UsersDBAccess));
    const handleRequestSpy = jest.spyOn(DataHandler.prototype, 'handleRequest');
    expect(handleRequestSpy).toBeCalled();
  });
});
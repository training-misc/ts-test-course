import { LoginHandler } from "../../app/Handlers/LoginHandler";
import { HTTP_CODES, HTTP_METHODS, SessionToken } from "../../app/Models/ServerModels";
import { Utils } from "../../app/Utils/Utils";

describe('LoginHandler test suite', () => {
	let loginHandler: LoginHandler; // #1
	// # 1
	const requestMock = {
		method: ''
	};
	// # 1
	const responseMock = {
		writeHead: jest.fn(), // #1
		write: jest.fn(), // #2
		statusCode: 0 // # 2
	};
	// # 1
	const authorizerMock = {
		generateToken: jest.fn() // # 2
	};
	//# 2
	const getRequestBodyMock = jest.fn();

	// # 2  
	//dummy session
	const someSessionToken: SessionToken = {
		accessRights: [1, 2, 3],
		expirationTime: new Date(),
		userName: 'someUserName',
		valid: true,
		tokenId: 'someTokenId'
	}

	beforeEach(() => {
		// # 1
		loginHandler = new LoginHandler(
			requestMock as any,
			responseMock as any,
			authorizerMock as any
		)
		// # 2
		Utils.getRequestBody = getRequestBodyMock;

		// # 3
		requestMock.method = HTTP_METHODS.POST;
	});

	// # 1
	afterEach(() => {
		jest.clearAllMocks();
	})

	// # 1
	// line 19
	it('options request', async () => {
		requestMock.method = HTTP_METHODS.OPTIONS;
		await loginHandler.handleRequest();
		expect(responseMock.writeHead).toBeCalledWith(HTTP_CODES.OK)
	});

	//# 1
	// line 31
	it('not handled http method', async () => {
		requestMock.method = 'someRandomMethod';
		await loginHandler.handleRequest();
		expect(responseMock.writeHead).not.toHaveBeenCalled();
	});

	// line 35 - 42
	it('post request with valid login', async () => {
		// requestMock.method = HTTP_METHODS.POST; #3
		getRequestBodyMock.mockReturnValueOnce({
			username: 'username',
			password: 'password'
		})
		authorizerMock.generateToken.mockReturnValueOnce(someSessionToken);
		await loginHandler.handleRequest();
		expect(responseMock.statusCode).toBe(HTTP_CODES.CREATED);
		expect(responseMock.writeHead).toBeCalledWith(
			HTTP_CODES.CREATED, { 'Content-Type': 'application/json' }
			);
		expect(responseMock.write).toBeCalledWith(JSON.stringify(someSessionToken));
	});

	//line 43 - 46 #3
	it('post request with invalid login', async () => {
		// requestMock.method = HTTP_METHODS.POST; #3
		getRequestBodyMock.mockReturnValueOnce({
			username: 'username',
			password: 'password'
		})
		authorizerMock.generateToken.mockReturnValueOnce(null);
		await loginHandler.handleRequest();
		expect(responseMock.statusCode).toBe(HTTP_CODES.NOT_fOUND);
		expect(responseMock.write).toBeCalledWith('wrong username or password');
	});

	// line 47 -50 #3
	it('post request with unexpected error', async () => {
		// requestMock.method = HTTP_METHODS.POST; #3
		getRequestBodyMock.mockRejectedValueOnce(new Error('something went wrong!'));
		await loginHandler.handleRequest();
		expect(responseMock.statusCode).toBe(HTTP_CODES.INTERNAL_SERVER_ERROR);
		expect(responseMock.write).toBeCalledWith('Internal error: something went wrong!');
	});

});
import { SessionTokenDBAccess } from "../../app/Authorization/SessionTokenDBAccess";
import * as Nedb from 'nedb';
import { SessionToken } from "../../app/Models/ServerModels";
jest.mock('nedb');

describe('SessionTokenDBAccess Test Suite', () => {
  let sessionTokenDBAccess: SessionTokenDBAccess;
  //# 1
  const nedbMock = {
    loadDatabase: jest.fn(),
    insert: jest.fn(),
    find: jest.fn()
  };

  //# 1
  const someToken: SessionToken = {
    tokenId: '123',
    userName: 'username',
    valid: true,
    expirationTime: new Date(),
    accessRights: [1, 2, 3]
  };

  // # 3
  const someTokenId = '123';

  beforeEach(() => {
    sessionTokenDBAccess = new SessionTokenDBAccess(nedbMock as any);
    expect(nedbMock.loadDatabase).toBeCalled();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  //#1 line 
  it('constructor argument', async () => {
    new SessionTokenDBAccess();
    expect(Nedb).toBeCalledWith('databases/sessionToken.db')
  });

  //# 1 line 13 - 15, 18-20
  it('store sessionToken without error', async () => {
    nedbMock.insert.mockImplementationOnce(
      (someToken: any, cb: any) => {
        cb()
      }
    );
    await sessionTokenDBAccess.storeSessionToken(someToken);
    expect(nedbMock.insert).toBeCalledWith(someToken, expect.any(Function));
  });

  // #2 line 16 - 17
  it('store sessionToken with error', async () => {
    nedbMock.insert.mockImplementationOnce(
      (someToken: any, cb: any) => {
        cb(new Error('something went wrong'));
      }
    );
    await expect(sessionTokenDBAccess.storeSessionToken(someToken))
      .rejects.toThrow('something went wrong');
    expect(nedbMock.insert).toBeCalledWith(someToken, expect.any(Function));
  });

  //# 3 line 25 - 27, line 33 - 35
  it('get token with result and no error', async () => {
    const bar = (someTokenId: string, cb: any) => {
      cb(null, [someToken])
    }
    nedbMock.find.mockImplementationOnce(bar);
    const getTokenResult = await sessionTokenDBAccess.getToken(someTokenId);
    expect(getTokenResult).toBe(someToken);
    expect(nedbMock.find).toBeCalledWith({ tokenId: someTokenId }, expect.any(Function));
  });

  // #3 line 31 - 32
  it('get token with no result and no error', async () => {
    const bar = (someTokenId: string, cb: any) => {
      cb(null, [])
    }
    nedbMock.find.mockImplementationOnce(bar);
    const getTokenResult = await sessionTokenDBAccess.getToken(someTokenId);
    expect(getTokenResult).toBeNull;
    expect(nedbMock.find).toBeCalledWith({ tokenId: someTokenId }, expect.any(Function));
  });

  //#3 line 28 - 29
  it('get token with error', async () => {
    const bar = (someTokenId: string, cb: any) => {
      cb(new Error("something went wrong"), [])
    }
    nedbMock.find.mockImplementationOnce(bar);
    await expect(sessionTokenDBAccess.getToken(someTokenId)).rejects.toThrow("something went wrong");
    expect(nedbMock.find).toBeCalledWith({ tokenId: someTokenId }, expect.any(Function));
  });

});

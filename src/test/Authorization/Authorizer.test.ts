import { Authorizer } from "../../app/Authorization/Authorizer";
import { SessionTokenDBAccess } from "../../app/Authorization/SessionTokenDBAccess";
import { UserCredentialsDbAccess } from "../../app/Authorization/UserCredentialsDbAccess";
import { SessionToken, Account, TokenState } from "../../app/Models/ServerModels";
// # 1 mock a module
jest.mock("../../app/Authorization/SessionTokenDBAccess");
jest.mock("../../app/Authorization/UserCredentialsDbAccess");

describe('Authorizer test suite', () => {
  let authorizer: Authorizer;

  //# 2
  const sessionTokenDBAccessMock = {
    storeSessionToken: jest.fn(),
    getToken: jest.fn()
  };
  const userCredentialsDBAccess = {
    getUserCredential: jest.fn()
  };

  // #3
  const someAccount: Account = {
    username: 'username',
    password: 'pasword'
  }

  beforeEach(() => {
    //authorizer = new Authorizer(); // #1
    //# 2
    authorizer = new Authorizer(
      sessionTokenDBAccessMock as any,
      userCredentialsDBAccess as any,
    );
  });

  // # 1
  afterEach(() => {
    jest.clearAllMocks();
  })

  // line 11 - 16 # 1
  it('constructor arguments', () => {
    new Authorizer()
    expect(SessionTokenDBAccess).toBeCalled();
    expect(SessionTokenDBAccess).toBeCalled();
  });

  // line 18 -31
  it('should return sessionToken for valid credentials', async () => {
    jest.spyOn(global.Math, 'random').mockReturnValueOnce(0);
    jest.spyOn(global.Date, 'now').mockReturnValueOnce(0);
    userCredentialsDBAccess.getUserCredential.mockResolvedValueOnce({
      username: 'username',
      accessRights: [1, 2, 3]
    });
    const expectedSessionToken: SessionToken = {
      accessRights: [1, 2, 3],
      expirationTime: new Date(60 * 60 * 1000),
      userName: 'username',
      valid: true,
      tokenId: ''
    }
    const sessionToken = await authorizer.generateToken(someAccount);
    expect(expectedSessionToken).toEqual(sessionToken)
    expect(sessionTokenDBAccessMock.storeSessionToken).toHaveBeenCalledWith(sessionToken)
  });

  // line 32-34
  it('should return null for invalid credentials', async () => {
    userCredentialsDBAccess.getUserCredential.mockReturnValue(null);
    const loginResult = await authorizer.generateToken(someAccount);
    expect(loginResult).toBeNull();
    expect(userCredentialsDBAccess.getUserCredential).toBeCalledWith(someAccount.username, someAccount.password);
  });

  // line 37-43
  it('validateToken returns invalid for null token', async () => {
    sessionTokenDBAccessMock.getToken.mockReturnValueOnce(null);
    const sessionToken = await authorizer.validateToken('123');
    expect(sessionToken).toStrictEqual({
      accessRights: [],
      state: TokenState.INVALID
    });
  });

  // line 44-48
  it('validateToken returns expired for expired tokens', async () => {
    const dateInPast = new Date(Date.now() - 1);
    sessionTokenDBAccessMock.getToken.mockReturnValueOnce({ valid: true, expirationTime: dateInPast });
    const sessionToken = await authorizer.validateToken('123');
    expect(sessionToken).toStrictEqual({
      accessRights: [],
      state: TokenState.EXPIRED
    });
  });

  // line 49-52
  it('validateToken returns valid for not expired and valid tokens', async () => {
    const dateInFuture = new Date(Date.now() + 100000);
    sessionTokenDBAccessMock.getToken.mockReturnValue(
      {
        valid: true,
        expirationTime: dateInFuture,
        accessRights: [1]
      });
    const sessionToken = await authorizer.validateToken('123');
    expect(sessionToken).toStrictEqual({
      accessRights: [1],
      state: TokenState.VALID
    });
  });

});
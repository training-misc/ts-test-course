import { mocked } from 'ts-jest/utils'; //#2 Typescript functionality
import { Launcher } from '../../app/Launcher';
import { Server } from '../../app/Server/Server';

//# 1 Set up the mock server
jest.mock('../../app/Server/Server', () => {
  return {
    Server: jest.fn(() => {
      return {
        startServer: () => {
          console.log('starting fake server!');
        }
      }
    })
  }
});

describe('Launcher test suite', () => {
  const mockedServer = mocked(Server, true);

  afterEach(() => {
    jest.clearAllMocks();
  })

  //# 1 This line completes the launcher code coverage
  // line 7-9
  it('create server', () => {
    new Launcher();
    //#2 
    expect(mockedServer).toBeCalled();
  });

  //# 3 line 10-12
  it('launch app', () => {
    const launchAppMock = jest.fn();
    Launcher.prototype.launchApp = launchAppMock;
    new Launcher().launchApp();
    expect(launchAppMock).toBeCalled();
  });

});
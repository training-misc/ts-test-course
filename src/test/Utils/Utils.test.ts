import { IncomingMessage } from 'http';
import { Utils } from '../../app/Utils/Utils'


describe('Utils test suite', () => {

	it('getRequestPath valid request', () => {
		const request = {
			url: 'https://localhost:8080/login'
		} as IncomingMessage;

		const resultPath = Utils.getRequestBasePath(request);
		expect(resultPath).toBe('login');
	});

	it('getRequestPath with no path name', () => {
		const request = {
			url: 'https://localhost:8080/'
		} as IncomingMessage;

		const resultPath = Utils.getRequestBasePath(request);
		expect(resultPath).toBeFalsy();
	});

});